 public string GenerarPaginacion(int amount, int pageOverride, string tblNombre, string method, out int total_paginas)
        {
            // Generando la estructura HTML para la paginación
            // http://www.tradingcode.net/csharp/operators/modulus-operator/
            int pag = pageOverride != 0 ? pageOverride : String.IsNullOrEmpty(ConfigurationManager.AppSettings["rowsperpagina"]) ? 15 : Convert.ToInt32(ConfigurationManager.AppSettings["rowsperpagina"]);
            int pg_div = amount / pag;
            int pg_mod = amount % pag;
            int pg_tot = 0;

            StringBuilder sbp = new StringBuilder();

            if (pg_div >= 1)
            {
                pg_tot = ((pg_div == 1 && pg_mod == 0) ? 0 : pg_div) + ((pg_mod > 0) ? 1 : 0);

                sbp.AppendLine("<li id='" + tblNombre + "_pg_inicio'><a aria-label='Inicio' onclick='CustomRetrocederTodo(\"#" + tblNombre + "\")'><span aria-hidden='true'>&laquo;</span></a></li>");
                sbp.AppendLine("<li id='" + tblNombre + "_pg_anterior'><a aria-label='Anterior' onclick='CustomRetroceder(\"#" + tblNombre + "\")'><span aria-hidden='true'>&lsaquo;</span></a></li>");

                for (int i = 0; i < pg_tot; i++)
                {
                    if (i == 0)
                    {
                        sbp.AppendLine("<li class='active'><a href='#' class='qolo' onclick='" + method + "(" + (i + 1) + ", \"#" + tblNombre + "\")'>" + (i + 1) + "</a></li>");
                    }
                    else
                    {
                        sbp.AppendLine("<li><a class='comunicamas' onclick='" + method + "(" + (i + 1) + ", \"#" + tblNombre + "\")'>" + (i + 1) + "</a></li>");
                    }
                }

                sbp.AppendLine("<li id='" + tblNombre + "_pg_siguiente'><a href='#' aria-label='Siguiente' onclick='CustomAvanzar(\"#" + tblNombre + "\")'><span aria-hidden='true'>&rsaquo;</span></a></li>");
                sbp.AppendLine("<li id='" + tblNombre + "_pg_final'><a href='#' aria-label='Final' onclick='CustomAvanzarTodo(\"#" + tblNombre + "\")'><span aria-hidden='true'>&raquo;</span></a></li>");

                total_paginas = pg_tot;

                return sbp.ToString();
            }

            total_paginas = 0;

            return "0";
        }
